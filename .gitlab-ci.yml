# Copyright 2018-2022 David Robillard <d@drobilla.net>
# SPDX-License-Identifier: 0BSD OR ISC

stages: [build, deploy]

default:
  stage: build
  image: lv2plugin/debian-x64
  script:
    - meson setup build -Dwerror=true
    - ninja -C build

dev:
  stage: build
  image: lv2plugin/debian-x64
  script:
    - meson setup build -Dbuildtype=debug -Ddocs=enabled -Ddocs_cpp=false -Dstrict=true -Dwerror=true
    - ninja -C build
    - meson configure -Dbuildtype=release build
    - ninja -C build
  artifacts:
    paths:
      - build/doc

static:
  stage: build
  image: lv2plugin/debian-x64
  script:
    - meson setup build -Ddefault_library=static -Ddocs=disabled -Dstrict=true -Dwerror=true
    - ninja -C build
    - meson configure -Dbuildtype=release build
    - ninja -C build

sanitize:
  stage: build
  image: lv2plugin/debian-x64-clang
  script:
    - meson setup build -Db_lundef=false -Dbuildtype=plain -Ddocs=disabled -Dstrict=true -Dwerror=true
    - ninja -C build
  variables:
    CC: "clang"
    CXX: "clang++"
    CFLAGS: "-fno-sanitize-recover=all -fsanitize=address -fsanitize=undefined -fsanitize=float-divide-by-zero -fsanitize=unsigned-integer-overflow -fsanitize=implicit-conversion -fsanitize=local-bounds -fsanitize=nullability"
    CXXFLAGS: "-fno-sanitize-recover=all -fsanitize=address -fsanitize=undefined -fsanitize=float-divide-by-zero -fsanitize=unsigned-integer-overflow -fsanitize=implicit-conversion -fsanitize=local-bounds -fsanitize=nullability"
    LDFLAGS: "-fno-sanitize-recover=all -fsanitize=address -fsanitize=undefined -fsanitize=float-divide-by-zero -fsanitize=unsigned-integer-overflow -fsanitize=implicit-conversion -fsanitize=local-bounds -fsanitize=nullability"

# Linux Platforms

arm32:
  stage: build
  image: lv2plugin/debian-arm32
  script:
    - meson setup build --cross-file=/usr/share/meson/cross/arm-linux-gnueabihf.ini -Dbuildtype=debug -Ddocs=disabled -Dstrict=true -Dwerror=true
    - ninja -C build
    - meson configure -Dbuildtype=release build
    - ninja -C build

arm64:
  stage: build
  image: lv2plugin/debian-arm64
  script:
    - meson setup build --cross-file=/usr/share/meson/cross/aarch64-linux-gnu.ini -Dbuildtype=debug -Ddocs=disabled -Dstrict=true -Dwerror=true
    - ninja -C build
    - meson configure -Dbuildtype=release build
    - ninja -C build

mingw32:
  stage: build
  image: lv2plugin/debian-mingw32
  script:
    - meson setup build --cross-file=/usr/share/meson/cross/i686-w64-mingw32.ini -Dbuildtype=debug -Ddocs=disabled -Dstrict=true -Dwerror=true
    - ninja -C build
    - meson configure -Dbuildtype=release build
    - ninja -C build
  variables:
    WINEPATH: "Z:\\usr\\lib\\gcc\\i686-w64-mingw32\\10-win32"

mingw64:
  stage: build
  image: lv2plugin/debian-mingw64
  script:
    - meson setup build --cross-file=/usr/share/meson/cross/x86_64-w64-mingw32.ini -Dbuildtype=debug -Ddocs=disabled -Dstrict=true -Dwerror=true
    - ninja -C build
    - meson configure -Dbuildtype=release build
    - ninja -C build

# Linux Distributions

fedora:
  stage: build
  image: lv2plugin/fedora-big
  script:
    - meson setup build -Dbuildtype=plain -Dstrict=true -Dwerror=true
    - ninja -C build
  variables:
    CFLAGS: -O2 -D_FORTIFY_SOURCE=2

# Non-Linux/Docker rows (not hosted)

mac_dbg:
  stage: build
  tags: [macos]
  script:
    - meson setup build -Dbuildtype=debug -Ddocs=disabled -Dstrict=true -Dwerror=true
    - ninja -C build

mac_rel:
  stage: build
  tags: [macos]
  script:
    - meson setup build -Dbuildtype=release -Ddocs=disabled -Dstrict=true -Dwerror=true
    - ninja -C build

win_dbg:
  stage: build
  tags: [windows,meson]
  script:
    - meson setup build -Dbuildtype=debug -Ddocs=disabled -Dstrict=true -Dwerror=true
    - ninja -C build

win_rel:
  stage: build
  tags: [windows,meson]
  script:
    - meson setup build -Dbuildtype=release -Ddocs=disabled -Dstrict=true -Dwerror=true
    - ninja -C build

pages:
  stage: deploy
  script:
    - mkdir public
    - mkdir public/c
    - mkdir public/cpp
    - mv build/doc/c/singlehtml/ public/c/singlehtml/
    - mv build/doc/cpp/singlehtml/ public/cpp/singlehtml/
    - mv build/doc/c/html/ public/c/html/
    - mv build/doc/cpp/html/ public/cpp/html/
  dependencies:
    - dev
  artifacts:
    paths:
      - public
  only:
    - master
